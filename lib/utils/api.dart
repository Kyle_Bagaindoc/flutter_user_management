import 'dart:convert';

import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
	Future getUsers() async {
		try {
			String url = 'https://jsonplaceholder.typicode.com/users';
			var response = await Dio().get(url);
			List<User> users = [];

			for (var user in response.data) {
				// The 'user' here is of Map<String, dynamic> type.
				// The 'user' map is converted into a User object.
				// After conversion, the object is added to the list.
				users.add(User.fromJson(user));
			}

			return users;
		} catch (exception) {
			throw exception;
		}
	}

	Future addUsers({
		required String name,
		required String email,
		required String phone,
		required String website

	}) async {
		try {
			String url = 'https://jsonplaceholder.typicode.com/users';
			var response = await Dio().post(url, 

		    data: jsonEncode( < String, String>{
                    'name': name,
                    'email': email,
                    'phone': phone,
                    'website': website
                })
            );
			return response;
		} catch (exception) {
			throw exception;
		}
        }

    Future updateUser({
        required int? id,
        required String name,
        required String email,
        required String phone,
        required String website

    }) async {
        try {
            String url = 'https://jsonplaceholder.typicode.com/users';
			Response response = await Dio().put(
                url + '/$id', 
                data: jsonEncode(<String, String>{
                    'name': name, 
                    'email': email, 
                    'phone': phone, 
                    'website': website
                }), 
            );
            return response;
        } catch (exception) {
            throw exception;
        }
    }
}





import 'dart:async';
import 'package:flutter/material.dart';

import '/models/user.dart';
import '/utils/api.dart';


class EditUserScreen extends StatefulWidget {
    final User? _user;

    EditUserScreen(this._user);

    @override
    _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
    Future? _futureUser;

    final _formKey = GlobalKey<FormState>();
    final _txtNameController = TextEditingController();
    final _txtEmailController = TextEditingController();
    final _txtPhoneController = TextEditingController();
    final _txtWebsiteController = TextEditingController();

    void updateUserDialog(BuildContext context) {
        setState(() {
            _futureUser = API().updateUser(
                id: widget._user!.id,
                name: _txtNameController.text, 
                email: _txtEmailController.text, 
                phone: _txtPhoneController.text, 
                website: _txtWebsiteController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }
    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);   
    }
    
    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget _name = TextFormField(
            decoration: InputDecoration(labelText: 'Name',),
            keyboardType: TextInputType.text,
            controller: _txtNameController,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget _email = TextFormField(
            decoration: InputDecoration(labelText: 'Email',),
            keyboardType: TextInputType.emailAddress,
            controller: _txtEmailController,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget _phone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone',),
            keyboardType: TextInputType.text,
            controller: _txtPhoneController,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget _website = TextFormField(
            decoration: InputDecoration(labelText: 'Website',),
            keyboardType: TextInputType.text,
            controller: _txtWebsiteController,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget btnEdit = Container(
            width: double.infinity,
            margin: EdgeInsets.all(5.0),
            child: SizedBox(
                child: ElevatedButton(
                    child: Text('Update'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            updateUserDialog(context);
                        } else {
                            showSnackBar(context, 'Error');
                        }
                    }
                ),
            ),
        );

        Widget userForm = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        _name,
                        _email,
                        _phone,
                        _website,
                        btnEdit
                    ],
                )
            ),
        );

        Widget updateView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot) {
                if (_futureUser == null) {
                    return userForm;
                } 
                
                if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
						showSnackBar(context, 'Could not update the user, restart the app.');
					} else if (snapshot.hasData){
                        print(snapshot.data);

                        Timer(Duration(seconds: 1), () {
                            showSnackBar(context, 'User updated successfully');
                            Navigator.pushNamed(context, '/');
                        });

                    } else {
                        return userForm;
                    }
                }

                return Center(
					child: CircularProgressIndicator()
				);
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Add User')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child:updateView
            ),
        );

    }
}

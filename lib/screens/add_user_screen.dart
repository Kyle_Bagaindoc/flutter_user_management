import 'dart:async';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import '/utils/api.dart';
class AddUserScreen extends StatefulWidget {
    @override
    AddUserScreenState createState() => AddUserScreenState();
}

class AddUserScreenState extends State<AddUserScreen> {
    Future? _futureUser;

    final _formKey = GlobalKey<FormState>();

    final _tffNameController = TextEditingController();
    final _tffEmailController = TextEditingController();
    final _tffPhoneController = TextEditingController();
    final _tffWebsiteController = TextEditingController();

    void addUser(BuildContext context){

        setState(() {
                _futureUser = API().addUsers(
                name: _tffNameController.text,
                email: _tffEmailController.text,
                phone: _tffPhoneController.text,
                website: _tffWebsiteController.text
                ).catchError((error){
                    showSnackBar(context, error.message);
            });
        
        });

    }
    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);   
    }
    @override
    Widget build(BuildContext context) {

        Widget _name = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Name',
                    labelStyle: TextStyle(
                    color: Colors.black
                    )    
                    
                ),
                keyboardType: TextInputType.text,
                controller: _tffNameController 
                     
        );
        Widget _email = TextFormField(
                decoration: InputDecoration(labelText: 'Email'),
                keyboardType: TextInputType.emailAddress,
                controller: _tffEmailController,
                  validator: (email) {      
                    if (email == null || email.isEmpty){
                        return 'The email must be provided.';
                    }    
                    if (EmailValidator.validate(email) == false){
                        return 'A valid email must be provided';
                    }
                    return null;
                  }


        );
        Widget _phone = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Phone',
                    labelStyle: TextStyle(
                    color: Colors.black
                    
                    )
                ),
                keyboardType: TextInputType.text,
                controller: _tffPhoneController
            
        );
        Widget _website = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Website',
                    labelStyle: TextStyle(
                    color: Colors.black
                    )
                    
                ),
                keyboardType: TextInputType.text,
                controller: _tffWebsiteController
        );
        Widget btnAdd = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Register'),
                onPressed: () {
                    addUser(context);
                }
            )
        );
        Widget AddUser = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        _name,
                        _email,
                        _phone,
                        _website,
                        btnAdd,
                        
                    ]
                )
            )
        );
        Widget AddView = FutureBuilder(
            future: _futureUser,
            builder:(context,snapshot){
                if(_futureUser == null){
                    return AddUser;
                }else if (snapshot.hasData == false){
                    print(snapshot.connectionState);
                    print(snapshot.hasData);
                    print(snapshot.hasError);
                    return AddUser;
                } else if(snapshot.hasError == true){
                    showSnackBar(context, 'Encountered an error');
                    return AddUser;
                }else {
                    Timer(Duration(seconds: 3),(){
                        print(snapshot.data);
                        showSnackBar(context, 'Success');
                        Navigator.pushReplacementNamed(context, '/');        
                    });
                } 
                return Center(
                        child:CircularProgressIndicator()
                    );
                }
        );
        return Scaffold(
            appBar: AppBar(title: Text('New User Registration')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: AddView
            )
        );
    }

}
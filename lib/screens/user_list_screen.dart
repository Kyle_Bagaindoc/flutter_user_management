import 'package:flutter/material.dart';

import '/screens/add_user_screen.dart';
import '/utils/api.dart';
import '/models/user.dart';
import 'edit_user_screen.dart';

class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
	Future? futureUsers;
	final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

	List<Widget> generateListTiles(List<User> users) {
		List<Widget> listTiles = [];

		for (User user in users) {
			listTiles.add(ListTile(
				title: Text(user.name),
				subtitle: Text(user.email),
                trailing: IconButton(
                    onPressed:(){
                        // var route = MaterialPageRoute(builder: (BuildContext context) =>);
                        
                        // individualUser =API().getUser(id: user.id);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => EditUserScreen(user)));
                        
                        // Navigator.of(context).push(route);
                    },
                    icon: Icon(Icons.edit), iconSize: 20,color: Colors.black,
                ), 
			));
		}

		return listTiles;
	}

	@override
	void initState() {
		super.initState();

		WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
			setState(() {
				futureUsers = API().getUsers();
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		Widget fbUserList = FutureBuilder(
			future: futureUsers,
			builder: (context, snapshot) {
				if (snapshot.connectionState == ConnectionState.done) {
					if (snapshot.hasError) {
						return Center(
							child: Text('Could not load the user list, restart the app.')
						);
					}

					return RefreshIndicator(
						key: refreshIndicatorKey,
						onRefresh: () async {
							setState(() {
								futureUsers = API().getUsers();
							});
						},
						child: ListView(
							padding: EdgeInsets.all(8.0),
							children: generateListTiles(snapshot.data as List<User>)
						)
					);
				}

				return Center(
					child: CircularProgressIndicator()
				);
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('User List')),
			body: Container(
				child: fbUserList
			),

          drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
                             hoverColor: Colors.brown[200],
                            title: Text('Add User'),
                            onTap: () {

                             {
                                    Navigator.pushNamed(context, '/add_user_screen'); 
                            }
                            }
                        ),
                        
                    ]
                )
            )
                
           
        );

	




	}
}